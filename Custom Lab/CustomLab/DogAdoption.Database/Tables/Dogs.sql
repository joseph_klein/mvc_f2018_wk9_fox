﻿CREATE TABLE [dbo].[Dogs]
(
	[DogId] INT NOT NULL IDENTITY(1,1),
	[DogImage] VARCHAR(80) NOT NULL,
	[Name] VARCHAR(20) NOT NULL,
	[isAvailable] BIT NOT NULL,
	[Breed] VARCHAR(25) NOT NULL,
	[Age] INT NOT NULL,
	[Comments] TEXT NULL,

	PRIMARY KEY ([DogId]),
)
