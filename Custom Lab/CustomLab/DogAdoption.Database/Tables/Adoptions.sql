﻿CREATE TABLE [dbo].[Adoptions]
(  
	[CustomerId] int NOT NULL,
    [DogId] int NOT NULL,
	[AdoptionDate] DATE NOT NULL,
	PRIMARY KEY ([CustomerId], [DogId]),
    FOREIGN KEY ([CustomerId]) REFERENCES Customers(CustomerId),
    FOREIGN KEY ([DogId]) REFERENCES Dogs(DogId),
)
