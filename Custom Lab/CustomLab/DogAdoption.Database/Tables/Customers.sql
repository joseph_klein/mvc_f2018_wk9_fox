﻿CREATE TABLE [dbo].[Customers]
(
	[CustomerId] INT NOT NULL IDENTITY(1,1),
	[CustomerImage] VARCHAR(80) NOT NULL,
	[Name] NVARCHAR(60) NOT NULL,
	[PhoneNumber] NVARCHAR(10) NOT NULL,
	[AddressOne] VARCHAR(40) NOT NULL,
	[AddressTwo] VARCHAR(15) NULL,
	[City] VARCHAR(30) NOT NULL,
	[State] CHAR(2) NOT NULL,
	[Zip] VARCHAR(20) NOT NULL,

	PRIMARY KEY ([CustomerId]),
)
