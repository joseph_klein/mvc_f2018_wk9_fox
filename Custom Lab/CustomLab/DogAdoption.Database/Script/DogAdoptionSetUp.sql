﻿CREATE TABLE Adoptions (
    CustomerId int NOT NULL,
    DogId int NOT NULL,
	PRIMARY KEY (Cust_Id, Dog_Id),
    CustomerId int FOREIGN KEY REFERENCES Customers(CustomerId),
    DogId int FOREIGN KEY REFERENCES Dogs(DogId)
);