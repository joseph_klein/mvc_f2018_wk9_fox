﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DogAdoption.Website.Models
{
    [MetadataType(typeof(DogsMetadata))]
    public partial class Dog
    {
    }
    public class DogsMetadata
    {
        [Key]
        [Display(Name = "#")]
        public int DogId { get; set; }
        
        [Display(Name = "Image")]
        [StringLength(80, ErrorMessage = "Must be less than 80 characters")]
        public string DogImage { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(20, ErrorMessage = "Must be less than 20 characters")]
        public string Name { get; set; }

        [Display(Name = "Available")]
        public bool isAvailable { get; set; }

        [Required(ErrorMessage = "Breed is required")]
        [StringLength(25, ErrorMessage = "Must be less than 25 characters")]
        public string Breed { get; set; }

        [Required(ErrorMessage = "Age is required")]
        [Range(0, 25, ErrorMessage = "Age must be between 0 and 25")]
        public int Age { get; set; }

        [DataType(DataType.Text)]
        [StringLength(200, ErrorMessage = "Must be less than 200 characters")]
        public string Comments { get; set; }
    }
}