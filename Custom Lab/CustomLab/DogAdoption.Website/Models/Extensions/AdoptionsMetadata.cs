﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DogAdoption.Website.Models
{

    [MetadataType(typeof(AdoptionsMetadata))]
    public partial class Adoption
    {
    }
    public class AdoptionsMetadata
    {
        [Display(Name = "Customer #")]
        public int CustomerId { get; set; }

        [Display(Name = "Dog #")]
        public int DogId { get; set; }
        
        [Display(Name = "Adoption Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Adoption date is required")]
        public DateTime AdoptionDate { get; set; }
    }
}