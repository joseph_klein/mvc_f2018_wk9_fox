﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DogAdoption.Website.Models
{
    [MetadataType(typeof(CustomersMetadata))]
    public partial class Customer
    {
    }
    public class CustomersMetadata
    {
        [Key]
        [Display(Name = "#")]
        public int CustomerId { get; set; }
        
        [Display(Name = "Image")]
        [StringLength(80, ErrorMessage = "Must be less than 80 characters")]
        public string CustomerImage { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Must be less than 60 characters")]
        public string Name { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone Number is required")]
        [StringLength(10, ErrorMessage = "Must be less than 10 characters")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address is required")]
        [StringLength(40, ErrorMessage = "Must be less than 40 characters")]
        public string AddressOne { get; set; }

        [Display(Name = "Apt. #")]
        [StringLength(15, ErrorMessage = "Must be less than 15 characters")]
        public string AddressTwo { get; set; }

        [Required(ErrorMessage = "City is required")]
        [StringLength(30, ErrorMessage = "Must be less than 30 characters")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required")]
        [StringLength(2, ErrorMessage = "Must be less than 2 characters")]
        public string State { get; set; }

        [Required(ErrorMessage = "Zip is required")]
        [StringLength(20, ErrorMessage = "Must be less than 20 characters")]
        public string Zip { get; set; }
    }
}