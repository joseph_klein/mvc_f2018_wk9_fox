﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DogAdoption.Website.Startup))]
namespace DogAdoption.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
