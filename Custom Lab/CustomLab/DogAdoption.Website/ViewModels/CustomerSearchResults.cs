﻿using DogAdoption.Website.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DogAdoption.Website.ViewModels
{
    public class CustomerSearchResults
    {
        public string Search { get; set; }
        public string SortBy { get; set; }
        
        public IPagedList<Customer> Customers { get; set; }
    }
}