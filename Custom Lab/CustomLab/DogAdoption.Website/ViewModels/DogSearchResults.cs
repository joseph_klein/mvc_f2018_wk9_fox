﻿using DogAdoption.Website.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogAdoption.Website.ViewModels
{
    public class DogSearchResults
    {
        public string Search { get; set; }
        public string SortBy { get; set; }

        public IPagedList<Dog> Dogs { get; set; }
    }
}