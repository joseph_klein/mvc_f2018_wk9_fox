﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogAdoption.Website
{
    public class Constants
    {
        public const int MAX_UPLOAD_FILE_SIZE = 2 * 1024 * 1024;
        public const string THUMBNAILS_FOLDER = "Thumbnails/";
        public const string CUSTOMERS_FOLDER_PATH = "~/Content/Customers/";
        public const string DOGS_FOLDER_PATH = "~/Content/Dogs/";

        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS =
        {
            ".png", ".jpg", ".jpeg"
        };
    }
}