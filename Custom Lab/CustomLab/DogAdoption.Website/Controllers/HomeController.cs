﻿using DogAdoption.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DogAdoption.Website.Controllers
{
    public class HomeController : Controller
    {
        private DogAdoptionEntities1 _db = new DogAdoptionEntities1();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
                _db = null;
            }
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }
        
    }
}