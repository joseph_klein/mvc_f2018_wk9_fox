﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using DogAdoption.Website.Models;
using DogAdoption.Website.ViewModels;
using NLog;
using PagedList;

namespace DogAdoption.Website.Controllers
{
    public class DogsController : Controller
    {
        private DogAdoptionEntities1 db = new DogAdoptionEntities1();
        private Logger logger = LogManager.GetCurrentClassLogger();


        public bool ValidateImage(string name, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(name, "Please select a file");
                return false;
            }

            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(name, "File size was zero");
                return false;
            }

            if (file.ContentLength >= Constants.MAX_UPLOAD_FILE_SIZE)
            {
                ModelState.AddModelError(name, "File size was to big");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(name, $"File extension {fileExtension} not allowed");
                return false;
            }

            return true;
        }

        private void UploadDogImage(Dog dog, HttpPostedFileBase file)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > 350 || img.Height > 600)
            {
                img.Resize(350, 600);
            }

            string fileName = file.FileName.ToLower();
            img.Save(Constants.DOGS_FOLDER_PATH + fileName);

            img.Resize(100, 100);
            img.Save(Constants.DOGS_FOLDER_PATH + Constants.THUMBNAILS_FOLDER + fileName);

            dog.DogImage = fileName;
        }
        // GET: Dogs
        [AllowAnonymous]
        public ActionResult Index(
            string search,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            IQueryable<Dog> dog =
                from d in db.Dogs
                orderby d.DogId, d.Name
                select d;

            dog = dog.Where(m => m.isAvailable == true);

            if (!String.IsNullOrWhiteSpace(search))
            {
                dog = dog.Where(
                       m => m.Name.Contains(search) ||
                       m.DogId.ToString().Contains(search)
                    );
            }

            switch (sortBy)
            {
                default:
                case "dogNumber":
                    dog = dog.OrderBy(m => m.DogId);
                    break;
                case "dogName":
                    dog = dog.OrderBy(m => m.Name);
                    break;
            }

            var model = new DogSearchResults()
            {
                Search = search,
                SortBy = sortBy,
                Dogs = dog.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            return View("Index", model);
        }

        // GET: Dogs/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dog dog = db.Dogs.Find(id);
            if (dog == null)
            {
                return HttpNotFound();
            }
            logger.Info($"{User.Identity.Name} viewed {dog.Name}");
            return View(dog);
        }

        // GET: Dogs/Create
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DogId,Name,isAvailable,Breed,Age,Comments,DogImage")]
        Dog dog,
            HttpPostedFileBase imageUpload)
        {
            try
            { 
                if (imageUpload != null &&
                   ValidateImage("DogImage", imageUpload))
                {
                    UploadDogImage(dog, imageUpload);
                }

                if (ModelState.IsValid)
                {
                    dog.isAvailable = true;
                    db.Dogs.Add(dog);
                    logger.Info($"{User.Identity.Name} created {dog.Name}");
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error creating {dog.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return View(dog);
        }

        // GET: Dogs/Edit/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dog dog = db.Dogs.Find(id);
            if (dog == null)
            {
                return HttpNotFound();
            }
            return View(dog);
        }

        // POST: Dogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DogId,Name,isAvailable,Breed,Age,Comments,DogImage")]
        Dog dog,
           HttpPostedFileBase imageUpload)
        {
            try
            {
                if (imageUpload != null &&
                   ValidateImage("DogImage", imageUpload))
                {
                    UploadDogImage(dog, imageUpload);
                }

                if (ModelState.IsValid)
                {
                    db.Entry(dog).State = EntityState.Modified;
                    logger.Info($"{User.Identity.Name} modified {dog.Name}");
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error editing {dog.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return View(dog);
        }

        // GET: Dogs/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dog dog = db.Dogs.Find(id);
            if (dog == null)
            {
                return HttpNotFound();
            }
            return View(dog);
        }

        // POST: Dogs/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { 
                Dog dog = db.Dogs.Find(id);
                db.Dogs.Remove(dog);
                logger.Info($"{User.Identity.Name} deleted {dog.Name}");
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error deleting {db.Dogs.Find(id).Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
