﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using DogAdoption.Website.Models;
using DogAdoption.Website.ViewModels;
using NLog;
using PagedList;

namespace DogAdoption.Website.Controllers
{
    public class CustomersController : Controller
    {
        private DogAdoptionEntities1 db = new DogAdoptionEntities1();
        private Logger logger = LogManager.GetCurrentClassLogger();

        public bool ValidateImage(string name, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(name, "Please select a file");
                return false;
            }

            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(name, "File size was zero");
                return false;
            }

            if (file.ContentLength >= Constants.MAX_UPLOAD_FILE_SIZE)
            {
                ModelState.AddModelError(name, "File size was to big");
                return false;
            }

            string fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(name, $"File extension {fileExtension} not allowed");
                return false;
            }

            return true;
        }

        private void UploadCustomerImage(Customer customer, HttpPostedFileBase file)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > 350 || img.Height > 600)
            {
                img.Resize(350, 600);
            }

            string fileName = file.FileName.ToLower();
            img.Save(Constants.CUSTOMERS_FOLDER_PATH + fileName);
            
            img.Resize(100, 100);
            img.Save(Constants.CUSTOMERS_FOLDER_PATH + Constants.THUMBNAILS_FOLDER + fileName);
            
            customer.CustomerImage = fileName;
        }

        // GET: Customers
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Index(
            string search,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            IQueryable<Customer> cust =
                from c in db.Customers
                orderby c.CustomerId, c.Name
                select c;

            if (!String.IsNullOrWhiteSpace(search))
            {
                cust = cust.Where(
                       m => m.Name.Contains(search) ||
                       m.CustomerId.ToString().Contains(search)
                    );
            }

            switch (sortBy)
            {
                default:
                case "customerNumber":
                    cust = cust.OrderBy(m => m.CustomerId);
                    break;
                case "customerName":
                    cust = cust.OrderBy(m => m.Name);
                    break;
            }

            var model = new CustomerSearchResults()
            {
                Search = search,
                SortBy = sortBy,
                Customers = cust.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            return View("Index", model);
        }

        // GET: Customers/Details/5
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            logger.Info($"{User.Identity.Name} viewed {customer.Name}");
            return View(customer);
        }

        // GET: Customers/Create
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Manager, Counselor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerId,Name,PhoneNumber,AddressOne,AddressTwo,City,State,Zip,CustomerImage")]
        Customer customer,
             HttpPostedFileBase imageUpload)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (imageUpload != null &&
                        ValidateImage("CustomerImage", imageUpload))
                    {
                        UploadCustomerImage(customer, imageUpload);
                    }

                    db.Customers.Add(customer);
                    logger.Info($"{User.Identity.Name} created {customer.Name}");
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            catch(Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error creating {customer.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return View(customer);
        }

        // GET: Customers/Edit/5
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Edit([Bind(Include = "CustomerId,Name,PhoneNumber,AddressOne,AddressTwo,City,State,Zip,CustomerImage")]
        Customer customer,
             HttpPostedFileBase imageUpload)
        {
            try
            { 
                if (imageUpload != null &&
                    ValidateImage("CustomerImage", imageUpload))
                {
                    UploadCustomerImage(customer, imageUpload);
                }

                if (ModelState.IsValid)
                {
                    db.Entry(customer).State = EntityState.Modified;
                    logger.Info($"{User.Identity.Name} modified {customer.Name}");
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error editing {customer.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { 
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            logger.Info($"{User.Identity.Name} deleted {customer.Name}");
            db.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error deleting {db.Customers.Find(id).Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
