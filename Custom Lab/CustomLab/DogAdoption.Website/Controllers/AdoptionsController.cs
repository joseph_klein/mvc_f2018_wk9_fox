﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DogAdoption.Website.Models;
using DogAdoption.Website.ViewModels;
using NLog;
using PagedList;

namespace DogAdoption.Website.Controllers
{
    public class AdoptionsController : Controller
    {
        private DogAdoptionEntities1 db = new DogAdoptionEntities1();
        private Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Adoptions
        [AllowAnonymous]
        public ActionResult Index(
            string search,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            IQueryable<Adoption> adopt =
                from a in db.Adoptions
                join c in db.Customers on a.CustomerId equals c.CustomerId
                join d in db.Dogs on a.DogId equals d.DogId
                orderby a.AdoptionDate
                select a;

            if (!String.IsNullOrWhiteSpace(search))
            {
                adopt = adopt.Where(
                       m => m.Customer.Name.Contains(search) ||
                       m.Dog.Name.Contains(search) ||
                       m.DogId.ToString().Contains(search) ||
                       m.CustomerId.ToString().Contains(search)
                    );
            }

            switch (sortBy)
            {
                default:
                case "date":
                    adopt = adopt.OrderBy(m => m.AdoptionDate);
                    break;
                case "dogName":
                    adopt = adopt.OrderBy(m => m.Dog.Name);
                    break;
                case "customerName":
                    adopt = adopt.OrderBy(m => m.Customer.Name);
                    break;
            }

            var model = new AdoptionSearchResults()
            {
                Search = search,
                SortBy = sortBy,
                Adoptions = adopt.ToPagedList(page ?? 1, itemsPerPage ?? 5)
            };

            return View("Index", model);
        }

        // GET: Adoptions/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id, int? dog)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id, dog);
            if (adoption == null)
            {
                return HttpNotFound();
            }
            logger.Info($"{User.Identity.Name} viewed the adoption of {adoption.Dog.Name}");
            return View(adoption);
        }

        // GET: Adoptions/Create
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Name");
            ViewBag.DogId = new SelectList(db.Dogs, "DogId", "Name");
            return View();
        }

        // POST: Adoptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Manager, Counselor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerId,DogId,AdoptionDate")] Adoption adoption)
        {
            try
            { 
                if (ModelState.IsValid)
                {
                    Dog dog = db.Dogs.Find(adoption.DogId);

                    if(dog.isAvailable == true)
                    {
                        dog.isAvailable = false;
                        db.Entry(dog).State = EntityState.Modified;
                        adoption.AdoptionDate = DateTime.Today;
                        db.Adoptions.Add(adoption);
                        logger.Info($"{User.Identity.Name} created the adoption of {adoption.Dog.Name}");
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }

                ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Name", adoption.CustomerId);
                ViewBag.DogId = new SelectList(db.Dogs, "DogId", "Name", adoption.DogId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error creating the adoption of {adoption.Dog.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return View(adoption);
        }

        // GET: Adoptions/Edit/5
        [Authorize(Roles = "Admin, Manager, Counselor")]
        public ActionResult Edit(int? id, int? dog)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id, dog);
            if (adoption == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Name", adoption.CustomerId);
            ViewBag.DogId = new SelectList(db.Dogs, "DogId", "Name", adoption.DogId);
            return View(adoption);
        }

        // POST: Adoptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin, Manager, Counselor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerId,DogId,AdoptionDate")] Adoption adoption)
        {
            try
            { 
                if (ModelState.IsValid)
                {
                    db.Entry(adoption).State = EntityState.Modified;
                    logger.Info($"{User.Identity.Name} modified the adoption of {adoption.Dog.Name}");
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Name", adoption.CustomerId);
                ViewBag.DogId = new SelectList(db.Dogs, "DogId", "Name", adoption.DogId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error editing the adoption of {adoption.Dog.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return View(adoption);
        }

        [Authorize(Roles = "Admin, Manager")]
        // GET: Adoptions/Delete/5
        public ActionResult Delete(int? id, int? dog)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id, dog);
            if (adoption == null)
            {
                return HttpNotFound();
            }
            return View(adoption);
        }

        // POST: Adoptions/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int dog)
        {
            try
            { 
                Adoption adoption = db.Adoptions.Find(id, dog);
                Dog _dog = db.Dogs.Find(adoption.DogId);
                _dog.isAvailable = true;
                db.Entry(_dog).State = EntityState.Modified;
                db.Adoptions.Remove(adoption);
                logger.Info($"{User.Identity.Name} removed the adoption of {adoption.Dog.Name}");
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.Name} had an error deleting the adoption of {db.Adoptions.Find(id, dog).Dog.Name}");
                ModelState.AddModelError("CustomerId", ex);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
