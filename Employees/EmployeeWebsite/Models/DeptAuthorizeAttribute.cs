﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeWebsite.Models
{
    [AttributeUsage(
        System.AttributeTargets.Class | System.AttributeTargets.Method,
        AllowMultiple = true, Inherited = true)]
    public class DeptAuthorizeAttribute : AuthorizeAttribute
    {
        public bool AllowAdmins { get; set; }
        public string Depts { get; set; }

        //public override void OnAuthorization(AuthorizationContext filterContext)
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
            {
                // User is not authenticated
                return false;
            }

            if (AllowAdmins && httpContext.User.IsInRole("Admin"))
            {
                return true;
            }

            var userId = httpContext.User.Identity.GetUserId();
            var userManager = httpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.FindById(userId);
            if (user != null)
            {
                string dept_name = GetDeptName(user);
                if (IsDeptAllowed(dept_name))
                {
                    return true;
                }
            }

            return false;
        }

        private string GetDeptName(ApplicationUser user)
        {
            using (var db = new EmployeeDatabase())
            {
                string dept_name = (
                    from e in db.employees
                    join d in db.departments on e.dept_no equals d.dept_no
                    where e.emp_no == user.emp_no
                    select d.dept_name
                ).FirstOrDefault();

                return dept_name;
            }
        }

        private bool IsDeptAllowed(string dept_name)
        {
            char[] seperators = new char[] { ',' };

            IEnumerable<string> depts =
                (Depts ?? "").Split(seperators, StringSplitOptions.RemoveEmptyEntries)
                             .Select(x => x.Trim());

            return depts.Contains(dept_name);
        }
    }
}