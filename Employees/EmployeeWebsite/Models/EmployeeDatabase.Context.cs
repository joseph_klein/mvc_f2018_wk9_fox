﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EmployeeWebsite.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class EmployeeDatabase : DbContext
    {
        public EmployeeDatabase()
            : base("name=EmployeeDatabase")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<department> departments { get; set; }
        public virtual DbSet<dept_emp> dept_emp { get; set; }
        public virtual DbSet<dept_manager> dept_manager { get; set; }
        public virtual DbSet<employee> employees { get; set; }
        public virtual DbSet<salary> salaries { get; set; }
        public virtual DbSet<title> titles { get; set; }
    
        public virtual ObjectResult<FindEmployeeByLastName_Result> FindEmployeeByLastName(string lastName)
        {
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<FindEmployeeByLastName_Result>("FindEmployeeByLastName", lastNameParameter);
        }
    }
}
