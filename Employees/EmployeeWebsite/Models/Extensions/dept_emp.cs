﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeWebsite.Models
{
    [MetadataType(typeof(dept_emp_metadata))]
    public partial class dept_emp
    {

    }

    public class dept_emp_metadata
    {
        [Display(Name = "Employee Number")]
        public int emp_no { get; set; }
        [Display(Name = "Department Number")]
        public string dept_no { get; set; }
        [Display(Name ="From Date")]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid Date")]
        public System.DateTime from_date { get; set; }
        [Display(Name = "To Date")]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid Date")]
        public System.DateTime to_date { get; set; }
    }
}