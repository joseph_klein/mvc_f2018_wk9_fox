﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeWebsite.Models
{
    [MetadataType(typeof(employee_metadata))]
    public partial class employee
    {
    }

    public class employee_metadata
    {
        [Display(Name = "Employee #")]
        public int emp_no { get; set; }

        [Display(Name = "Birth Date")]
        [DataType(DataType.Date, ErrorMessage = "Invalid Date.")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime birth_date { get; set; }

        [Display(Name = "First Name")]
        [Required]
        [StringLength(50, ErrorMessage = "The name you have written cannot exceed 50 characters.")]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        [StringLength(50, ErrorMessage = "The name you have written cannot exceed 50 characters.")]
        public string last_name { get; set; }

        [Display(Name = "Gender")]
        public string gender { get; set; }

        [Display(Name = "Hire Date")]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime hire_date { get; set; }

        [Display(Name = "Image")]
        [DataType(DataType.ImageUrl)]
        [StringLength(200, ErrorMessage = "The image cannot exceed 200 characters.")]
        public string employee_image { get; set; }
    }
}