﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmployeeWebsite.Models
{
    public class EmployeeInfo
    {
        [Display(Name = "Employee Number")]
        public int emp_no { get; set; }
        [Display(Name = "Last Name")]
        public string last_name { get; set; }
        [Display(Name = "First Name")]
        public string first_name { get; set; }
        [Display(Name = "Hire Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime hire_date { get; set; }
        [Display(Name = "Birth Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime birth_date { get; set; }
        [Display(Name = "dept_name")]
        public string dept_name { get; set; }
        [Display(Name = "Department Number")]
        public string dept_no { get; set; }

        [Display(Name = "Image")]
        public string employee_image { get; set; }
    }
}