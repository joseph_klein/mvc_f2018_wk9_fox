﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeWebsite
{
    public static class Constants
    {
        public const int ITEMS_PER_PAGE = 5;

        public const string EMPLOYEE_IMAGE_PATH = "~/Content/EmployeeImages/";
        public const string UPLOAD_IMAGE_PATH = "~/Content/Uploads/";

        public const int MAX_IMAGE_FILE_SIZE = 2097152;
        public const int THUMBNAIL_SIZE = 128;
        public const string THUMBNAIL_FOLDER_NAME = "Thumbnails/";

        public const int MAX_UPLOAD_IMAGE_WIDTH = 1024;
        public const int MAX_UPLOAD_IMAGE_HEIGHT = 768;

        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS = new string[]
        {
            ".gif", ".png", ".jpeg", ".jpg"
        };
    }
}