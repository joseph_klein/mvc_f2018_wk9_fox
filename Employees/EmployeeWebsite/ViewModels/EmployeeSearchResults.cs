﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EmployeeWebsite.Models;

namespace EmployeeWebsite.ViewModels
{
    public class EmployeeSearchResults
    {
        [Display(Name = "Search")]
        public string Search { get; set; }
        [Display(Name = "Department")]
        public string DepartmentId { get; set; }
        [Display(Name = "Release Year")]
        public int? MinYear { get; set; }
        [Display(Name = "Release Year")]
        public int? MaxYear { get; set; }
        [Display(Name = "Sort By")]
        public string SortBy { get; set; }

        public List<department> Departments { get; set; }
        public IPagedList<EmployeeInfo> Results { get; set; }
        
    }
}