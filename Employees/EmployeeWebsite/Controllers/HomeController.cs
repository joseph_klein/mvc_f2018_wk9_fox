﻿using EmployeeWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmployeeWebsite.Controllers
{
    public class HomeController : Controller
    {
        private EmployeeDatabase _db = new EmployeeDatabase();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
                _db = null;
            }
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            var departments =
                from d in _db.departments
                orderby d.dept_name
                select d;
            return View("Index", departments.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}