﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using EmployeeWebsite.Models;
using EmployeeWebsite.ViewModels;
using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.Owin;
using NLog;
using PagedList;

namespace EmployeeWebsite.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeDatabase db = new EmployeeDatabase();
        private Logger logger = LogManager.GetCurrentClassLogger();

        [Authorize]
        public ActionResult Index(
            string search,
            string dept_no,
            int? minYear,
            int? maxYear,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            var employees =
                from e in db.employees
                join d in db.departments on e.dept_no equals d.dept_no
                select new EmployeeInfo
                {
                    emp_no = e.emp_no,
                    last_name = e.last_name,
                    first_name = e.first_name,
                    hire_date = e.hire_date,
                    dept_name = d.dept_name,
                    dept_no = d.dept_no,
                    employee_image = e.employee_image
                };



            var userId = User.Identity.GetUserId();
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = userManager.FindById(userId);
            if (!User.IsInRole("HR") || !User.IsInRole("Admin"))
            {
                var emp = db.employees.Find(user.emp_no);
                if (emp != null)
                {
                    employees = employees.Where(m => m.dept_no == emp.dept_no);
                }
            }


            //Filter the results
            if (!string.IsNullOrWhiteSpace(dept_no))
            {
                employees = employees.Where(e => e.dept_no == dept_no);
            }
            if (minYear != null)
            {
                employees = employees.Where(e => e.hire_date.Year >= minYear);
            }
            if (maxYear != null)
            {
                employees = employees.Where(e => e.hire_date.Year <= maxYear);
            }

            //Search by keyword
            if (!String.IsNullOrWhiteSpace(search))
            {
                employees = employees.Where(
                    e => e.dept_no.Contains(search) ||
                         e.first_name.Contains(search) ||
                         e.last_name.Contains(search)
                );
            }

            employees = employees.OrderBy(e => e.emp_no);

            switch (sortBy)
            {
                default:
                case "employeeNumber":
                    employees = employees.OrderBy(e => e.emp_no);
                    break;
                case "lastName":
                    employees = employees.OrderBy(e => e.last_name);
                    break;
                case "firstName":
                    employees = employees.OrderBy(e => e.first_name);
                    break;
                case "hireDate":
                    employees = employees.OrderBy(e => e.hire_date);
                    break;
                case "department":
                    employees = employees.OrderBy(e => e.dept_name);
                    break;
            }

            var model = new EmployeeSearchResults
            {
                Search = search,
                DepartmentId = dept_no,
                MinYear = minYear,
                MaxYear = maxYear,
                SortBy = sortBy,
                Departments = db.departments.OrderBy(d => d.dept_name).ToList(),
                Results = employees.ToPagedList(page ?? 1, itemsPerPage ?? 3),
            };
            return View("Index", model);
        }

        private void PopulateDropDowns(EmployeeEditModel viewModel)
        {
            IQueryable<string> employeeImages = (
                from e in db.employees
                where !string.IsNullOrEmpty(e.employee_image)
                orderby e.employee_image
                select e.employee_image
           ).Distinct();

            viewModel.EmployeeImages = new SelectList(employeeImages);
        }

        private bool ValidateImageFile(string key, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError(key, "Please select a file");
                return false;
            }

            if(file.ContentLength <= 0)
            {
                ModelState.AddModelError(key, "file size was zero");
                return false;
            }
            
            if (file.ContentLength > Constants.MAX_IMAGE_FILE_SIZE)
            {
                ModelState.AddModelError(key, $"File was bigger than {Constants.MAX_IMAGE_FILE_SIZE} bytes.");
                return false;
            }

            string fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(key, $"File extension {fileExtension} not allowed");
                return false;
            }

            return true;
        }

        private void UploadImage(
            HttpPostedFileBase file,
            string destinationFolder,
            string filename,
            int maxWidth,
            int maxHeight)
        {
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > maxWidth || img.Height > maxHeight)
            {
                img.Resize(maxWidth, maxHeight);
            }
            img.Save(destinationFolder + filename);

            img.Resize(Constants.THUMBNAIL_SIZE, Constants.THUMBNAIL_SIZE);
            img.Save(destinationFolder + Constants.THUMBNAIL_FOLDER_NAME + filename);
        }

        private void UploadEmployeeImage(EmployeeEditModel viewModel)
        {
            if (viewModel.EmployeeImageFile != null &&
                ValidateImageFile("EmployeeImageFile", viewModel.EmployeeImageFile))
            {
                try
                {
                    UploadImage(
                        file: viewModel.EmployeeImageFile,
                        destinationFolder: Constants.EMPLOYEE_IMAGE_PATH,
                        filename: viewModel.employee_image,
                        maxWidth: Constants.MAX_UPLOAD_IMAGE_WIDTH,
                        maxHeight: Constants.MAX_UPLOAD_IMAGE_HEIGHT);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(
                        "EmployeeImage",
                        "Sorry, an error occurred with uploading the iamge, please try again.");
                }
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            logger.Info($"{User.Identity.GetUserName()} viewed {employee.first_name} {employee.last_name}");
            return View(employee);
        }

        // GET: Employee2/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "emp_no,birth_date,first_name,last_name,gender,hire_date,dept_no")] employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.employees.Add(employee);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                logger.Info($"{User.Identity.GetUserName()} has created {employee.first_name} {employee.last_name}");
                return View(employee);
            }
            catch(Exception ex)
            {
                logger.Error(ex, $"{User.Identity.GetUserName()} has failed to create {employee.first_name} {employee.last_name}");
                ModelState.AddModelError("Emp_No", ex);
            }
            return View(employee);
        }

        // GET: Employee2/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }

            EmployeeEditModel viewModel = new EmployeeEditModel
            {
                emp_no = employee.emp_no,
                first_name = employee.first_name,
                last_name = employee.last_name,
                hire_date = employee.hire_date,
                birth_date = employee.birth_date,
                gender = employee.gender,
                dept_no = employee.dept_no,
                employee_image = employee.employee_image
            };

            PopulateDropDowns(viewModel);
            return View(viewModel);
        }

        // POST: Employee2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeEditModel viewModel)
        {
            var employeeToUpdate = db.employees.Find(viewModel.emp_no);
            try
            {
                string[] propertiesToUpdate = new string[]
                {
                "emp_no",
                "birth_date",
                "first_name",
                "last_name",
                "gender",
                "hire_date",
                "dept_no",
                "employee_image"
                };
                if (TryUpdateModel(employeeToUpdate, "", propertiesToUpdate))
                {
                    if (ModelState.IsValid)
                    {
                        UploadEmployeeImage(viewModel);
                    }
                    if (ModelState.IsValid)
                    {
                        db.SaveChanges();
                        return RedirectToAction("Index", "Employee");
                    }
                }
                PopulateDropDowns(viewModel);
                logger.Info($"{User.Identity.GetUserName()} has edited {employeeToUpdate.first_name} {employeeToUpdate.last_name}");
               
            }
            catch(Exception ex)
            {
                logger.Error(ex, $"{User.Identity.GetUserName()} has failed to edit {employeeToUpdate.first_name} {employeeToUpdate.last_name}");
                ModelState.AddModelError("Emp_No", ex);
            }

            return View(viewModel);
        }

        // GET: Employee2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employee2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                employee employee = db.employees.Find(id);
                db.employees.Remove(employee);
                db.SaveChanges();
                logger.Info($"{User.Identity.GetUserName()} has deleted {employee.first_name} {employee.last_name}");
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                logger.Error(ex, $"{User.Identity.GetUserName()} failed to delete Employee {id}");
                ModelState.AddModelError("Emp_No", ex);
            }
            return View(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
