﻿CREATE TABLE [dbo].[titles] (
    [emp_no]    INT          NOT NULL,
    [title]     VARCHAR (50) NOT NULL,
    [from_date] DATE         NOT NULL,
    [to_date]   DATE         NULL,
    PRIMARY KEY CLUSTERED ([emp_no] ASC, [title] ASC, [from_date] ASC),
    FOREIGN KEY ([emp_no]) REFERENCES [dbo].[employees] ([emp_no]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20180607-112832]
    ON [dbo].[titles]([emp_no] ASC, [from_date] ASC)
    INCLUDE([to_date]);

