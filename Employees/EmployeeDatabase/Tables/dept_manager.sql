﻿CREATE TABLE [dbo].[dept_manager] (
    [emp_no]    INT      NOT NULL,
    [dept_no]   CHAR (4) NOT NULL,
    [from_date] DATE     NOT NULL,
    [to_date]   DATE     NOT NULL,
    PRIMARY KEY CLUSTERED ([emp_no] ASC, [dept_no] ASC),
    FOREIGN KEY ([dept_no]) REFERENCES [dbo].[departments] ([dept_no]) ON DELETE CASCADE,
    FOREIGN KEY ([emp_no]) REFERENCES [dbo].[employees] ([emp_no]) ON DELETE CASCADE
);

