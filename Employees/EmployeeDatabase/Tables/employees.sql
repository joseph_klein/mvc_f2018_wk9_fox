﻿CREATE TABLE [dbo].[employees] (
    [emp_no]     INT          NOT NULL,
    [birth_date] DATE         NOT NULL,
    [first_name] VARCHAR (14) NOT NULL,
    [last_name]  VARCHAR (16) NOT NULL,
    [gender]     CHAR (1)     NOT NULL,
    [hire_date]  DATE         NOT NULL,
	[dept_no]  CHAR(4)		  NULL,
	[employee_image] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([emp_no] ASC),
    CHECK ([gender]='F' OR [gender]='M')
);

