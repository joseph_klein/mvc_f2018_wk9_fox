﻿CREATE TABLE [dbo].[salaries] (
    [emp_no]    INT  NOT NULL,
    [salary]    INT  NOT NULL,
    [from_date] DATE NOT NULL,
    [to_date]   DATE NOT NULL,
    PRIMARY KEY CLUSTERED ([emp_no] ASC, [from_date] ASC),
    FOREIGN KEY ([emp_no]) REFERENCES [dbo].[employees] ([emp_no]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20180607-112950]
    ON [dbo].[salaries]([emp_no] ASC, [from_date] ASC)
    INCLUDE([to_date]);

