﻿CREATE TABLE [dbo].[departments] (
    [dept_no]   CHAR (4)     NOT NULL,
    [dept_name] VARCHAR (40) NOT NULL,
    PRIMARY KEY CLUSTERED ([dept_no] ASC),
    UNIQUE NONCLUSTERED ([dept_name] ASC)
);

