﻿
namespace LB01
{
    public static class Constants
    {
        public const int ITEMS_PER_PAGE = 5;
        public const int MAX_CAST_SIZE = 3;

        public const string GENRE_IMAGE_PATH = "~/Content/GenreImages/";
        public const string MOVIE_IMAGE_PATH = "~/Content/MovieImages/";
        public const string ACTOR_IMAGE_PATH = "~/Content/ActorImages/";
        public const string UPLOAD_IMAGE_PATH = "~/Content/Uploads/";

        public const int MAX_IMAGE_FILE_SIZE = 2097152;
        public const int THUMBNAIL_IMAGE_SIZE = 128;
        public const string THUMBNAIL_FOLDER_NAME = "Thumbnails/";

        public const int MAX_UPLOAD_IMAGE_WIDTH = 1024;
        public const int MAX_UPLOAD_IMAGE_HEIGHT = 768;
        public const int MAX_MOVIE_IMAGE_WIDTH = 450;
        public const int MAX_MOVIE_IMAGE_HEIGHT = 700;
        public const int MAX_ACTOR_IMAGE_SIZE = 128;

        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS = new string[]
        {
            ".gif", ".png", ".jpeg", ".jpg"
        };
    }
}