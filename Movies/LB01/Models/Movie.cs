﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LB01.Models
{
    [Table("Movies")]
    public class Movie
    {
        [Key]
        [Display(Name = "ID")]
        [Required(ErrorMessage = "Movie ID is required.")]
        public int MovieId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Movie name is required.")]
        [MinLength(3, ErrorMessage = "The name must be at least 3 characters long.")]
        [StringLength(200, ErrorMessage = "The name cannot exceed 200 characters.")]
        public string MovieName { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Movie description is required.")]
        [MinLength(3, ErrorMessage = "The description must be at least 3 characters long.")]
        [DataType(DataType.MultilineText)]
        public string MovieDescription { get; set; }

        [Display(Name = "Image")]
        [StringLength(200, ErrorMessage = "The image name cannot exceed 200 characters.")]
        public string MovieImage { get; set; }

        [Display(Name = "Released")]
        [Required(ErrorMessage = "Release year is required.")]
        [Range(1900, 2100, ErrorMessage = "The release year must be between {1} and {2}.")]
        public int ReleasedYear { get; set; }

        [Display(Name = "Ticket Sales")]
        [DisplayFormat(DataFormatString = "{0:0,,,.0} B")]
        [Range(0, 100000000000, ErrorMessage = "Ticket sales must be between {1} and {2}.")]
        [DataType(DataType.Currency)]
        public decimal? TicketSales { get; set; }

        [Display(Name = "Genre")]
        [Required(ErrorMessage = "Genre is required.")]
        public byte GenreId { get; set; }
        public virtual Genre Genre { get; set; }

        [Display(Name = "Cast")]
        public virtual ICollection<Actor> Cast { get; set; }

        [Display(Name = "Ratings")]
        public virtual IList<MovieRating> CustomerRatings { get; set; }

        [NotMapped]
        [Display(Name = "Rating")]
        public double AverageRating
        {
            get
            {
                if (CustomerRatings.Any())
                {
                    return CustomerRatings.Average(x => x.Rating);
                    //for (int i = 0; i < CustomerRatings.Count; ++i)
                    //{
                    //    r += CustomerRatings[i].Rating;
                    //};

                    //return r / CustomerRatings.Count;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}