﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace LB01.Models
{
    public class MovieDatabase : IdentityDbContext<ApplicationUser>
    {
        public static MovieDatabase Create()
        {
            return new MovieDatabase();
        }

        public MovieDatabase()
        {
            Database.Log = (sql => Trace.WriteLine(sql));
        }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<MovieRating> MovieRatings { get; set; }
    }
}