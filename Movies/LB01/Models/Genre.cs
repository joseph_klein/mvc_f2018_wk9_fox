﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LB01.Models
{
    [Table("Genres")]
    public class Genre
    {
        [Key]
        [Display(Name = "ID")]
        [Required(ErrorMessage = "Genre ID is required.")]
        public byte GenreId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Genre name is required.")]
        [StringLength(32, ErrorMessage = "The genre name cannot exceed 32 characters.")]
        public string GenreName { get; set; }

        [StringLength(200, ErrorMessage = "The image name cannot exceed 200 characters.")]
        public string GenreImage { get; set; }
    }
}