﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LB01.Models
{
    [Table("MovieRating")]
    public class MovieRating
    {
        [Key, Column(Order = 0)]
        public int MovieId { get; set; }

        [Key, Column(Order = 1)]
        public string UserId { get; set; }

        [Column(Order = 2)]
        [Display(Name = "User Rating")]
        [Range(0, 5, ErrorMessage = "Must give rating up to 5 and above 0.")]
        public byte Rating { get; set; }
    }
}