﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LB01.Models
{
    [Table("Actors")]
    public class Actor
    {
        [Key]
        [Display(Name = "ID")]
        [Required(ErrorMessage = "ID is required.")]
        public int ActorId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required.")]
        [MinLength(3, ErrorMessage = "The name must be at least 3 characters long.")]
        [StringLength(32, ErrorMessage = "The name cannot exceed 32 characters.")]
        public string ActorName { get; set; }

        [Display(Name = "Role")]
        [Required(ErrorMessage = "Role is required.")]
        [MinLength(3, ErrorMessage = "The role must be at least 3 characters long.")]
        [StringLength(32, ErrorMessage = "The role cannot exceed 32 characters.")]
        public string ActorRole { get; set; }

        [Display(Name = "Image")]
        [StringLength(200, ErrorMessage = "The image name cannot exceed 200 characters.")]
        public string ActorImage { get; set; }
        
        [Display(Name = "Movie")]
        [Required(ErrorMessage = "Movie is required.")]
        public int MovieId { get; set; }
        public virtual Movie Movie { get; set; }
    }
}