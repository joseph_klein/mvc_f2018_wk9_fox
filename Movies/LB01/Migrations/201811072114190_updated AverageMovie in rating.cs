namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedAverageMovieinrating : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "MovieRating", c => c.Byte());
            AddColumn("dbo.Movies", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Movies", "Discriminator");
            DropColumn("dbo.Movies", "MovieRating");
        }
    }
}
