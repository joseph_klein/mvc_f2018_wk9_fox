namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingvalidationcontstraints : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Genres", "GenreName", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.Movies", "MovieName", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Movies", "MovieDescription", c => c.String(nullable: false));
            AlterColumn("dbo.Movies", "TicketSales", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Movies", "TicketSales", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Movies", "MovieDescription", c => c.String());
            AlterColumn("dbo.Movies", "MovieName", c => c.String());
            AlterColumn("dbo.Genres", "GenreName", c => c.String());
        }
    }
}
