namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingActors : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actors",
                c => new
                    {
                        ActorId = c.Int(nullable: false, identity: true),
                        ActorName = c.String(nullable: false, maxLength: 32),
                        ActorRole = c.String(nullable: false, maxLength: 32),
                        ActorImage = c.String(maxLength: 200),
                        MovieId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ActorId)
                .ForeignKey("dbo.Movies", t => t.MovieId, cascadeDelete: true)
                .Index(t => t.MovieId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Actors", "MovieId", "dbo.Movies");
            DropIndex("dbo.Actors", new[] { "MovieId" });
            DropTable("dbo.Actors");
        }
    }
}
