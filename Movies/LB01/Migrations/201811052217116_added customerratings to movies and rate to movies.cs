namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcustomerratingstomoviesandratetomovies : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MovieRating", "MovieId");
            AddForeignKey("dbo.MovieRating", "MovieId", "dbo.Movies", "MovieId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MovieRating", "MovieId", "dbo.Movies");
            DropIndex("dbo.MovieRating", new[] { "MovieId" });
        }
    }
}
