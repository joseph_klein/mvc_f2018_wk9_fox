namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedMovieandRatingNavigationproperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MovieRating", "MovieId", "dbo.Movies");
            DropIndex("dbo.MovieRating", new[] { "MovieId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.MovieRating", "MovieId");
            AddForeignKey("dbo.MovieRating", "MovieId", "dbo.Movies", "MovieId", cascadeDelete: true);
        }
    }
}
