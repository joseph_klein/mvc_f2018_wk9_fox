// <auto-generated />
namespace LB01.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddedMovieRatinsandratinginmovie : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedMovieRatinsandratinginmovie));
        
        string IMigrationMetadata.Id
        {
            get { return "201811072137021_Added MovieRatins and rating in movie"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
