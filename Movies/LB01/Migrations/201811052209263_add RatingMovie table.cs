namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRatingMovietable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MovieRating",
                c => new
                    {
                        MovieId = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Rating = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.MovieId, t.UserId });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MovieRating");
        }
    }
}
