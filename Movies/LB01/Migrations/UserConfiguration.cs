namespace LB01.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class UserConfiguration : DbMigrationsConfiguration<LB01.Models.ApplicationDbContext>
    {
        public UserConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "LB01.Models.ApplicationDbContext";
        }

        protected override void Seed(LB01.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
