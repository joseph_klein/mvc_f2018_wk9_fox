namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removingdiscriminator : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MovieRating", "MovieId");
            AddForeignKey("dbo.MovieRating", "MovieId", "dbo.Movies", "MovieId", cascadeDelete: true);
            DropColumn("dbo.Movies", "MovieRating");
            DropColumn("dbo.Movies", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movies", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Movies", "MovieRating", c => c.Byte());
            DropForeignKey("dbo.MovieRating", "MovieId", "dbo.Movies");
            DropIndex("dbo.MovieRating", new[] { "MovieId" });
        }
    }
}
