namespace LB01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingimages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Genres", "GenreImage", c => c.String(maxLength: 200));
            AddColumn("dbo.Movies", "MovieImage", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Movies", "MovieImage");
            DropColumn("dbo.Genres", "GenreImage");
        }
    }
}
