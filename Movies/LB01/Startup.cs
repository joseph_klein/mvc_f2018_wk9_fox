﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LB01.Startup))]
namespace LB01
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
