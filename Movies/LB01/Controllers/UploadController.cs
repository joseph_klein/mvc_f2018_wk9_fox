﻿using System;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LB01.Controllers
{
    public class UploadController : Controller
    {
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase myfile)
        {
            if (ValidateImageFile("myfile", myfile))
            {
                try
                {
                    UploadImage(
                        file: myfile,
                        destinationFolder: Constants.UPLOAD_IMAGE_PATH,
                        filename: myfile.FileName,
                        maxWidth: Constants.MAX_UPLOAD_IMAGE_WIDTH,
                        maxHeight: Constants.MAX_UPLOAD_IMAGE_HEIGHT);
                    ViewBag.Message = $"{myfile.FileName} uploaded";
                }
                catch (Exception)
                {
                    ModelState.AddModelError(
                        "myfile",
                        "Sorry, an error occurred with uploading the image, please try again.");
                }
            }
            return View();
        }
        
        private bool ValidateImageFile(string key, HttpPostedFileBase file)
        {
            // Check that a file was selected
            if (file == null)
            {
                ModelState.AddModelError(key, "Please select a file");
                return false;
            }

            // Check the the image file isn't to small or too big
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(key, "File size was zero");
                return false;
            }
            if (file.ContentLength > Constants.MAX_IMAGE_FILE_SIZE)
            {
                ModelState.AddModelError(key, $"File was bigger than {Constants.MAX_IMAGE_FILE_SIZE} bytes.");
                return false;
            }

            // Check that the image has the proper extension
            string fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(key, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadImage(
            HttpPostedFileBase file,
            string destinationFolder,
            string filename,
            int maxWidth,
            int maxHeight)
        {
            // Save the image
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > maxWidth || img.Height > maxHeight)
            {
                img.Resize(maxWidth, maxHeight);
            }
            img.Save(destinationFolder + filename);

            // Save the thumbnail
            img.Resize(Constants.THUMBNAIL_IMAGE_SIZE, Constants.THUMBNAIL_IMAGE_SIZE);
            img.Save(destinationFolder + Constants.THUMBNAIL_FOLDER_NAME + filename);
        }
    }
}