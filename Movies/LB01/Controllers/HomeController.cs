﻿using LB01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LB01.Controllers
{
    public class HomeController : Controller
    {
        private MovieDatabase _db = new MovieDatabase();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
                _db = null;
            }
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            var genres =
                from g in _db.Genres
                orderby g.GenreName
                select g;
            
            return View("Index", genres.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}