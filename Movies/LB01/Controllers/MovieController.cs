﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using LB01.Models;
using LB01.ViewModels;
using Microsoft.AspNet.Identity;
using NLog;
using PagedList;

namespace LB01.Controllers
{
    public class MovieController : Controller
    {
        private MovieDatabase db = new MovieDatabase();
        private Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Movie
        [AllowAnonymous]
        public ActionResult Index(
            string search,
            int? genreId,
            int? minYear,
            int? maxYear,
            string sortBy,
            int? page,
            int? itemsPerPage)
        {
            IQueryable<Movie> movies = db.Movies.Include("Genre");

            // Filter the results
            if (genreId != null)
            {
                movies = movies.Where(m => m.GenreId == genreId);
            }
            if (minYear != null)
            {
                movies = movies.Where(m => m.ReleasedYear >= minYear);
            }
            if (maxYear != null)
            {
                movies = movies.Where(m => m.ReleasedYear <= maxYear);
            }

            // Search by keyword
            if (!String.IsNullOrWhiteSpace(search))
            {
                movies = movies.Where(
                    m => m.MovieName.Contains(search) ||
                         m.MovieDescription.Contains(search) ||
                         m.ReleasedYear.ToString().Contains(search) ||
                         m.Genre.GenreName.Contains(search)
                );
            }

            // Sort the results
            switch (sortBy)
            {
                default:
                case "name":
                    movies = movies.OrderBy(m => m.MovieName);
                    break;
                case "year":
                    movies = movies.OrderBy(m => m.ReleasedYear).ThenBy(m => m.MovieName);
                    break;
                case "genre":
                    movies = movies.OrderBy(m => m.Genre.GenreName).ThenBy(m => m.MovieName);
                    break;
            }

            // Display the results
            var model = new MovieSearchResults
            {
                Search = search,
                GenreId = genreId,
                MinYear = minYear,
                MaxYear = maxYear,
                SortBy = sortBy,
                Genres = db.Genres.OrderBy(g => g.GenreName).ToList(),
                Results = movies.ToPagedList(page ?? 1, itemsPerPage ?? Constants.ITEMS_PER_PAGE),
            };

            return View("Index", model);
        }

        // GET: Movie/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            movie.Cast = movie.Cast.OrderBy(x => x.ActorId).ToList();

            MovieRating movieRating = db.MovieRatings.Find(movie.MovieId, User.Identity.GetUserId());
            MovieRatingEdit model = new MovieRatingEdit()
            {
                MovieId = movie.MovieId,
                MovieName = movie.MovieName,
                MovieDescription = movie.MovieName,
                MovieImage = movie.MovieImage,
                ReleasedYear = movie.ReleasedYear,
                TicketSales = movie.TicketSales,
                GenreId = movie.GenreId,
                Genre = movie.Genre,
                Cast = movie.Cast,
                CustomerRatings = movie.CustomerRatings,
                MovieRating = 0
            };

            if (movieRating != null)
            {
                model.MovieRating = movieRating.Rating;
            }

            logger.Info($"{User.Identity.GetUserName()} viewed {movie.MovieName}");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Reviewer")]
        public ActionResult AddingRating(int? movieId, byte? movieRating)
        {
            string userId = User.Identity.GetUserId();
            MovieRating thisRate = db.MovieRatings.Find(movieId, userId);


            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(userId)
                    && movieRating != null &&
                    movieId != null
                    )
                {

                    if (thisRate != null)
                    {
                        thisRate.Rating = (byte)movieRating;
                    }
                    else
                    {
                        MovieRating movieRate = new MovieRating()
                        {
                            MovieId = (int)movieId,
                            UserId = userId,
                            Rating = (byte)movieRating
                        };


                        db.MovieRatings.Add(movieRate);
                    }
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Details", new { id = movieId });
        }

        // GET: Movie/Create
        [Authorize(Roles = "Admin,Author")]
        public ActionResult Create()
        {
            MovieEditModel viewModel = new MovieEditModel();

            // Build an empty cast list
            viewModel.Cast = new List<ActorEditModel>();
            for (int i = 0; i < Constants.MAX_CAST_SIZE; ++i)
            {
                viewModel.Cast.Add(new ActorEditModel());
            }

            viewModel.ReleasedYear = DateTime.Now.Year;
            PopulateDropDowns(viewModel);
            return View(viewModel);
        }

        // POST: Movie/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Author")]
        public ActionResult Create(MovieEditModel viewModel)
        {
            try
            {
                Movie movieToCreate = new Movie()
                {
                    MovieId = viewModel.MovieId,
                    MovieName = viewModel.MovieName,
                    MovieDescription = viewModel.MovieDescription,
                    MovieImage = viewModel.MovieImage,
                    ReleasedYear = viewModel.ReleasedYear,
                    TicketSales = viewModel.TicketSales,
                    GenreId = viewModel.GenreId
                };
                db.Movies.Add(movieToCreate);

                // Add actors to the cast
                var cast = viewModel.Cast.Where(
                    x => !string.IsNullOrWhiteSpace(x.ActorName) ||
                            !string.IsNullOrWhiteSpace(x.ActorRole) ||
                            !string.IsNullOrWhiteSpace(x.ActorImage)
                ).ToList();
                for (int i = 0; i < Constants.MAX_CAST_SIZE && i < cast.Count; ++i)
                {
                    if (ValidateActor(cast, i))
                    {
                        var actorViewModel = cast[i];
                        var actor = new Actor();
                        actor.ActorName = actorViewModel.ActorName;
                        actor.ActorRole = actorViewModel.ActorRole;
                        actor.ActorImage = actorViewModel.ActorImage;
                        actor.Movie = movieToCreate;
                        db.Actors.Add(actor);
                    }
                }

                // Only save the movie if everything is valid
                if (ModelState.IsValid)
                {
                    // Upload the movie image if needed
                    UploadMovieImage(viewModel);
                    // Upload the actor images
                    UploadActorImages(cast);

                    // Save the movie to the database and display it if the images where uploaded successfully
                    if (ModelState.IsValid)
                    {
                        logger.Info($"{User.Identity.GetUserName()} created {movieToCreate.MovieName}");
                        db.SaveChanges();
                        return RedirectToAction("Details", new { id = movieToCreate.MovieId });
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex, $"{User.Identity.GetUserName()} had an error creating {viewModel.MovieName}");
                ModelState.AddModelError("MovieName", ex);
                //return new HttpStatusCodeResult(500);
            }

            PopulateDropDowns(viewModel);
            return View(viewModel);
        }

        // GET: Movie/Edit/5
        [Authorize(Roles = "Admin,Author")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }

            MovieEditModel viewModel = new MovieEditModel()
            {
                MovieId = movie.MovieId,
                MovieName = movie.MovieName,
                MovieDescription = movie.MovieDescription,
                MovieImage = movie.MovieImage,
                ReleasedYear = movie.ReleasedYear,
                TicketSales = movie.TicketSales,
                GenreId = movie.GenreId
            };

            // Load the current cast
            viewModel.Cast = (
                from a in db.Actors
                where a.MovieId == movie.MovieId
                orderby a.ActorId
                select new ActorEditModel
                {
                    ActorId = a.ActorId,
                    ActorName = a.ActorName,
                    ActorRole = a.ActorRole,
                    ActorImage = a.ActorImage,
                }
            ).Take(Constants.MAX_CAST_SIZE).ToList();
            for (int i = viewModel.Cast.Count; i < Constants.MAX_CAST_SIZE; ++i)
            {
                viewModel.Cast.Add(new ActorEditModel());
            }

            PopulateDropDowns(viewModel);
            return View(viewModel);
        }

        // POST: Movie/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Author")]
        public ActionResult Edit(MovieEditModel viewModel)
        {
            try
            {
                var movieToUpdate = db.Movies.Include(x => x.Cast)
                                           .Where(x => x.MovieId == viewModel.MovieId)
                                           .Single();

                // Update the movie's properties
                string[] propertiesToUpdate = new string[] {
                    "MovieId",
                    "MovieDescription",
                    "MovieImage",
                    "ReleasedYear",
                    "TicketSales",
                    "GenreId",
            };
                if (TryUpdateModel(movieToUpdate, "", propertiesToUpdate))
                {
                    // Upload the movie image if needed
                    if (ModelState.IsValid)
                    {
                        UploadMovieImage(viewModel);
                    }
                    // Update the movie's properties
                    if (ModelState.IsValid)
                    {
                        db.SaveChanges();
                    }

                    // Update the cast list

                    var updatedCast = viewModel.Cast.Where(
                        x => !string.IsNullOrWhiteSpace(x.ActorName) ||
                                !string.IsNullOrWhiteSpace(x.ActorRole) ||
                                !string.IsNullOrWhiteSpace(x.ActorImage)
                    ).Take(Constants.MAX_CAST_SIZE).ToList();

                    var currentCast = movieToUpdate.Cast.OrderBy(x => x.ActorId).ToList();

                    int i = 0;
                    for (; i < updatedCast.Count; ++i)
                    {
                        if (ValidateActor(updatedCast, i))
                        {
                            var actorViewModel = updatedCast[i];
                            if (i < currentCast.Count)
                            {
                                // Update an existing actor
                                var actor = currentCast[i];
                                actor.ActorName = actorViewModel.ActorName;
                                actor.ActorRole = actorViewModel.ActorRole;
                                actor.ActorImage = actorViewModel.ActorImage;
                            }
                            else
                            {
                                // Add a new actor
                                var actor = new Actor();
                                actor.ActorName = actorViewModel.ActorName;
                                actor.ActorRole = actorViewModel.ActorRole;
                                actor.ActorImage = actorViewModel.ActorImage;
                                actor.Movie = movieToUpdate;
                                db.Actors.Add(actor);
                            }
                        }
                    }
                    for (; i < currentCast.Count; ++i)
                    {
                        // Remove an existing actor
                        db.Actors.Remove(currentCast[i]);
                    }

                    // Only save the cast list if everything is valid
                    if (ModelState.IsValid)
                    {
                        // Upload the actor images
                        UploadActorImages(updatedCast);
                        if (ModelState.IsValid)
                        {
                            // Save the updated cast list
                            db.SaveChanges();

                            logger.Info($"{User.Identity.GetUserName()} created {movieToUpdate.MovieName}");
                            // Send the user back to the details page after their changes are saved
                            return RedirectToAction("Details", new { id = movieToUpdate.MovieId });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, $"{User.Identity.GetUserName()} had an error editing {viewModel.MovieName}");
                ModelState.AddModelError("MovieName", ex);
            }
            PopulateDropDowns(viewModel);
            return View(viewModel);

        }

        // GET: Movie/Delete/5
        [Authorize(Roles = "Admin,Author")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Author")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Movie movie = db.Movies.Find(id);
                db.Movies.Remove(movie);
                db.SaveChanges();
                logger.Info($"{User.Identity.GetUserName()} deleted {movie.MovieName}");
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                logger.Error(ex, $"{User.Identity.GetUserName()} had an error deleting {db.Movies.Find(id).MovieName}");
                ModelState.AddModelError("MovieName", ex);
            }
            return View(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PopulateDropDowns(MovieEditModel viewModel)
        {
            IQueryable<string> movieImages = (
                from m in db.Movies
                where !string.IsNullOrEmpty(m.MovieImage)
                orderby m.MovieImage
                select m.MovieImage
            ).Distinct();
            IQueryable<string> actorImages = (
                from a in db.Actors
                where !string.IsNullOrEmpty(a.ActorImage)
                orderby a.ActorImage
                select a.ActorImage
            ).Distinct();

            viewModel.Genres = new SelectList(db.Genres, "GenreId", "GenreName", viewModel.GenreId);
            viewModel.MovieImages = new SelectList(movieImages);
            viewModel.ActorImages = new SelectList(actorImages);
        }

        private bool ValidateActor(List<ActorEditModel> cast, int i)
        {
            string baseKey = $"Cast[{i}]";
            var actor = cast[i];
            bool valid = true;

            if (string.IsNullOrWhiteSpace(actor.ActorName))
            {
                ModelState.AddModelError(baseKey + ".ActorName", "Name is required.");
                valid = false;
            }
            if (string.IsNullOrWhiteSpace(actor.ActorRole))
            {
                ModelState.AddModelError(baseKey + ".ActorRole", "Role is required.");
                valid = false;
            }
            if (!string.IsNullOrWhiteSpace(actor.ActorImage) &&
                actor.ActorImageFile != null &&
                !ValidateImageFile(baseKey + ".ActorImageFile", actor.ActorImageFile))
            {
                valid = false;
            }

            return valid;
        }

        private void UploadMovieImage(MovieEditModel viewModel)
        {
            if (viewModel.MovieImageFile != null &&
                ValidateImageFile("MovieImageFile", viewModel.MovieImageFile))
            {
                try
                {
                    UploadImage(
                        file: viewModel.MovieImageFile,
                        destinationFolder: Constants.MOVIE_IMAGE_PATH,
                        filename: viewModel.MovieImage,
                        maxWidth: Constants.MAX_MOVIE_IMAGE_WIDTH,
                        maxHeight: Constants.MAX_MOVIE_IMAGE_HEIGHT);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(
                        "MovieImageFile",
                        "Sorry, an error occurred with uploading the image, please try again.");
                }
            }
        }

        private void UploadActorImages(List<ActorEditModel> cast)
        {
            for (int i = 0; i < cast.Count; ++i)
            {
                var actor = cast[i];
                if (actor.ActorImageFile != null)
                {
                    try
                    {
                        UploadImage(
                            file: actor.ActorImageFile,
                            destinationFolder: Constants.ACTOR_IMAGE_PATH,
                            filename: actor.ActorImage,
                            maxWidth: Constants.MAX_ACTOR_IMAGE_SIZE,
                            maxHeight: Constants.MAX_ACTOR_IMAGE_SIZE);
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError(
                            $"Cast[{i}].ActorImageFile",
                            "Sorry, an error occurred with uploading the image, please try again.");
                    }
                }
            }
        }

        private bool ValidateImageFile(string key, HttpPostedFileBase file)
        {
            // Check that a file was selected
            if (file == null)
            {
                ModelState.AddModelError(key, "Please select a file");
                return false;
            }

            // Check the the image file isn't to small or too big
            if (file.ContentLength <= 0)
            {
                ModelState.AddModelError(key, "File size was zero");
                return false;
            }
            if (file.ContentLength > Constants.MAX_IMAGE_FILE_SIZE)
            {
                ModelState.AddModelError(key, $"File was bigger than {Constants.MAX_IMAGE_FILE_SIZE} bytes.");
                return false;
            }

            // Check that the image has the proper extension
            string fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
            if (!Constants.ALLOWED_IMAGE_EXTENSIONS.Contains(fileExtension))
            {
                ModelState.AddModelError(key, $"File extension {fileExtension} not allowed.");
                return false;
            }

            return true;
        }

        private void UploadImage(
            HttpPostedFileBase file, 
            string destinationFolder,
            string filename,
            int maxWidth, 
            int maxHeight)
        {
            // Save the image
            WebImage img = new WebImage(file.InputStream);
            if (img.Width > maxWidth || img.Height > maxHeight)
            {
                img.Resize(maxWidth, maxHeight);
            }
            img.Save(destinationFolder + filename);

            // Save the thumbnail
            img.Resize(Constants.THUMBNAIL_IMAGE_SIZE, Constants.THUMBNAIL_IMAGE_SIZE);
            img.Save(destinationFolder + Constants.THUMBNAIL_FOLDER_NAME + filename);
        }
    }
}
