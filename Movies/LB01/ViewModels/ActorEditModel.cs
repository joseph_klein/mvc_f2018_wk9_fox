﻿using LB01.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace LB01.ViewModels
{
    public class ActorEditModel
    {
        [Key]
        [Display(Name = "ID")]
        public int ActorId { get; set; }

        [Display(Name = "Name")]
        [MinLength(3, ErrorMessage = "The name must be at least 3 characters long.")]
        [StringLength(32, ErrorMessage = "The name cannot exceed 32 characters.")]
        public string ActorName { get; set; }

        [Display(Name = "Role")]
        [MinLength(3, ErrorMessage = "The role must be at least 3 characters long.")]
        [StringLength(32, ErrorMessage = "The role cannot exceed 32 characters.")]
        public string ActorRole { get; set; }

        [Display(Name = "Image")]
        [StringLength(200, ErrorMessage = "The image name cannot exceed 200 characters.")]
        public string ActorImage { get; set; }
        public HttpPostedFileBase ActorImageFile { get; set; }
    }
}