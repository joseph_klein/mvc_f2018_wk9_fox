﻿using LB01.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace LB01.ViewModels
{
    public class MovieEditModel
    {
        [Key]
        [Display(Name = "ID")]
        [Required(ErrorMessage = "Movie ID is required.")]
        public int MovieId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Movie name is required.")]
        [MinLength(3, ErrorMessage = "The name must be at least 3 characters long.")]
        [StringLength(200, ErrorMessage = "The name cannot exceed 200 characters.")]
        public string MovieName { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Movie description is required.")]
        [MinLength(3, ErrorMessage = "The description must be at least 3 characters long.")]
        [DataType(DataType.MultilineText)]
        public string MovieDescription { get; set; }

        [Display(Name = "Image")]
        [StringLength(200, ErrorMessage = "The image name cannot exceed 200 characters.")]
        public string MovieImage { get; set; }
        public HttpPostedFileBase MovieImageFile { get; set; }

        [Display(Name = "Released")]
        [Required(ErrorMessage = "Release year is required.")]
        [Range(1900, 2100, ErrorMessage = "The release year must be between {1} and {2}.")]
        public int ReleasedYear { get; set; }

        [Display(Name = "Ticket Sales")]
        [DisplayFormat(DataFormatString = "{0:0,,,.0} B")]
        [Range(0, 100000000000, ErrorMessage = "Ticket sales must be between {1} and {2}.")]
        [DataType(DataType.Currency)]
        public decimal? TicketSales { get; set; }

        [Display(Name = "Genre")]
        [Required(ErrorMessage = "Genre is required.")]
        public byte GenreId { get; set; }

        [Display(Name = "Cast")]
        public virtual IList<ActorEditModel> Cast { get; set; }

        public SelectList Genres { get; set; }
        public SelectList MovieImages { get; set; }
        public SelectList ActorImages { get; set; }
    }
}