﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LB01.Models;
using PagedList;

namespace LB01.ViewModels
{
    public class MovieSearchResults
    {
        [Display(Name = "Search")]
        public string Search { get; set; }
        [Display(Name = "Genre")]
        public int? GenreId { get; set; }
        [Display(Name = "Release Year")]
        [Range(1900, 2100)]
        public int? MinYear { get; set; }
        [Display(Name = "Release Year")]
        [Range(1900, 2100)]
        public int? MaxYear { get; set; }
        [Display(Name = "Sort By")]
        public string SortBy { get; set; }

        public IEnumerable<Genre> Genres { get; set; }
        public IPagedList<Movie> Results { get; set; }
    }
}